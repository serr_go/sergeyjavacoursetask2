package com.sergey.javacourse.task2;

import com.sergey.javacourse.task2.CurrencyData.CurrenciesStorage;
import com.sergey.javacourse.task2.CurrencyData.CurrenciesStorageFiller;
import com.sergey.javacourse.task2.Utils.Approximator;

import java.util.ArrayList;

public class TaskTwoCurrency {
    private static final int DAYS = 30;

    //USD code in table
    private static final int CODE = 4;

    public static void main(String[] args)  {
        CurrenciesStorage storage = new CurrenciesStorage();
        CurrenciesStorageFiller currenciesStorageFiller = new CurrenciesStorageFiller();
        ParallelRequester parallelRequester = new ParallelRequester(storage);
        Approximator approximator = new Approximator();

        currenciesStorageFiller.fillDays(storage,DAYS);

        ArrayList<Thread> threads = new ArrayList<>();

        for (int i = 0; i < DAYS; i++) {
            threads.add(new Thread(parallelRequester));
        }

        for (Thread v : threads) {
            v.start();
        }

        for (Thread v : threads) {
            try {
                v.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }

        currenciesStorageFiller.fillCurrenciesExRate(storage,DAYS,parallelRequester.getAllResponses());

        double[] exchangeRates = new double[DAYS];
        for (int i = 0; i < DAYS; i++) {
            exchangeRates[i] =
                    storage.getCurrencyList().get(storage.getDaysList()
                            .get(i)).get(CODE).getExchangeRate();
        }
        System.out.print("Tomorrow exchange rate of USD will be:"
                +approximator.approximate(DAYS,exchangeRates));

    }
}
