package com.sergey.javacourse.task2.CurrencyData;

import com.sergey.javacourse.task2.Parsers.Parser;
import com.sergey.javacourse.task2.Parsers.ParserFactory;

import java.util.ArrayList;
import java.util.Calendar;

import static com.sergey.javacourse.task2.Parsers.ParserType.*;

public class CurrenciesStorageFiller {
    private ParserFactory factory = new ParserFactory();
    private Parser parser = factory.getParser(JSOUP);

    /**
     * Filling storage days.
     *
     * @param storage
     * @param days
     */
    public void fillDays(CurrenciesStorage storage, int days) {
        Calendar currentDate = Calendar.getInstance();
        String date;
        for (int i = 0; i < days; i++) {
            date = (currentDate.get(Calendar.YEAR) + "-"
                    + (currentDate.get(Calendar.MONTH) + 1) + "-"
                    + currentDate.get(Calendar.DATE));

            storage.setDaysList(date);

            currentDate.set(currentDate.get(Calendar.YEAR),
                    currentDate.get(Calendar.MONTH),
                    currentDate.get(Calendar.DATE) - 1);
        }
    }

    /**
     * Getting responses, parsing them and setting data in currencies list.
     *
     * @param storage
     * @param days
     * @param allResponses
     */
    public void fillCurrenciesExRate(CurrenciesStorage storage, int days, ArrayList<String> allResponses) {
        for (int i = 0; i < days; i++) {
            storage.setCurrenciesList(storage.getDaysList().get(i), parser.parse(allResponses.get(i)));
        }
    }
}
