package com.sergey.javacourse.task2.CurrencyData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CurrenciesStorage {
    private Map<String, ArrayList<Currency>> currenciesList = new HashMap<>();
    private ArrayList<String> daysList = new ArrayList<>();

    public Map<String, ArrayList<Currency>> getCurrencyList() {
        return currenciesList;
    }

    public ArrayList<String> getDaysList() {
        return daysList;
    }

    public void setCurrenciesList(String date, ArrayList<Currency> dailyRecord) {
        currenciesList.put(date,dailyRecord);
    }

    public void setDaysList (String date) {
        daysList.add(date);
    }
}