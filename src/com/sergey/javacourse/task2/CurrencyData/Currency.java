package com.sergey.javacourse.task2.CurrencyData;

public class Currency {
    private String code;
    private int amount;
    private String name;
    private double exchangeRate;

    public Currency(String code,int amount,String name,double exchangeRates) {
        this.code = code;
        this.amount = amount;
        this.name = name;
        this.exchangeRate = exchangeRates;
    }

    public String getCode() {
        return code;
    }

    public int getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }
}