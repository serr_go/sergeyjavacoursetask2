package com.sergey.javacourse.task2.Parsers;

import com.sergey.javacourse.task2.CurrencyData.Currency;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.ArrayList;

public class JsoupNBRBParser extends Parser {
    private final String CURRENCY_TABLE_ID = "BodyHolder_gvMain";
    private final String TABLE_ROW_TAG = "tr";
    private final String TABLE_CELL_TAG = "td";

    /**
     * JsoupNBRBParser
     * Makes HTML NBRB currency page parsing
     * using jsoup library
     *
     * @param response
     * @return ArrayList of daily records.
     */
    @Override
    public ArrayList<Currency> parse(String response) {
        ArrayList<Currency> dailyRecord = new ArrayList<>();
        Document doc = Jsoup.parse(response);

        Element table = doc.getElementById(CURRENCY_TABLE_ID);
        Elements rows = table.getElementsByTag(TABLE_ROW_TAG);

        for (int i = 1; i < rows.size(); i++) {
            Elements line = rows.get(i).getElementsByTag(TABLE_CELL_TAG);
            double exchangeRate = Double.parseDouble(line.get(3).text().replace(',','.'));
            dailyRecord.add(
                    new Currency(
                            line.get(0).text(),
                            Integer.parseInt(line.get(1).text()),
                            line.get(2).text(),
                            exchangeRate));
        }
        return new ArrayList<>(dailyRecord);
    }
}