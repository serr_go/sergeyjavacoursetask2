package com.sergey.javacourse.task2.Parsers;

public class ParserFactory {

    public Parser getParser(ParserType type){
        switch (type) {
            case REGEX:
                return new RegexNBRBParser();
            case JSOUP:
                return new JsoupNBRBParser();
            default:
                return null;
        }
    }
}
