package com.sergey.javacourse.task2.Parsers;

public enum ParserType {
    REGEX,
    JSOUP
}