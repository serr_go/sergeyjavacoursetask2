package com.sergey.javacourse.task2.Utils;

public class Approximator {
    public String approximate(int days, double[] currencyList) {

        int sumX = 0;
        double sumY = 0;
        double sumXY = 0;
        int sumXX = 0;
        double A, B;

        for (int i = 0; i < days; i++) {
            sumX += i;
            sumY += currencyList[i];
            sumXY += i*currencyList[i];
            sumXX += i*i;
        }

        A = (days*sumXY - sumX*sumY) / (days*sumXX - sumX*sumX);
        B = (sumY - A*sumX) / days;

        return String.format("%.4f", A*(days+1)+B);
    }
}