package com.sergey.javacourse.task2.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Requester {
    private final String USER_AGENT = "Mozilla/5.0";
    private final String REQUEST_METHOD = "GET";
    private int responseCode = 0;

    public int getResponseCode() {
        return responseCode;
    }

    /**
     * Sending GET request to pageURL
     *
     * @param pageURL
     * @return HTML page converted into String
     */
    public String sendGETRequest(String pageURL) throws RequestException {
        URL page = null;
        HttpURLConnection connection = null;
        BufferedReader pageData = null;
        String inputLine;
        StringBuffer response = new StringBuffer();

        try {
            page = new URL(pageURL);
            connection = (HttpURLConnection) page.openConnection();
            connection.setRequestMethod(REQUEST_METHOD);

            connection.setRequestProperty("User-Agent", USER_AGENT);

            responseCode = connection.getResponseCode();
            if (responseCode != 200) throw new RequestException("Request error "+ responseCode+".",responseCode);
            System.out.println("Response Code : " + responseCode);

            pageData = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            while ((inputLine = pageData.readLine()) != null) {
                response.append(inputLine);
            }
            pageData.close();

        } catch (MalformedURLException urlErr) {
            urlErr.printStackTrace();
        } catch (IOException openConnectErr) {
            openConnectErr.printStackTrace();
        }

        return response.toString();
    }
}

