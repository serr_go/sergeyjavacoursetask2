package com.sergey.javacourse.task2.Utils;

public class RequestException extends Exception{

    private int number;
    public int getNumber(){
        return number;
    }
    public RequestException(String message, int num){
        super(message);
        number=num;
    }
}
